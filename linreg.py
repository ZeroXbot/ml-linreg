import numpy as np
import random

# data = {(x, y)}

class LossP2:
    def __init__(self, data):
        self.data = data

    def loss(self, cofactors):
        X = np.insert(self.data[:, 1:], 0, 1, axis=1)
        Y = self.data[:, 0]
        count = self.data.shape[0]
        return np.sum(np.power(Y - np.dot(X, cofactors), 2)) / count

    def gradient(self, cofactors):
        X = np.insert(self.data[:, 1:], 0, 1, axis=1)
        Y = self.data[:, 0]
        count = self.data.shape[0]
        return -np.sum((Y - np.dot(X, cofactors)).reshape((count,1)) * X, axis=0) / count

class LossLogP2:
    def __init__(self, data):
        self.data = data

    def loss(self, cofactors):
        X = np.insert(self.data[:, 1:], 0, 1, axis=1)
        logY = np.log1p(self.data[:, 0])
        count = self.data.shape[0]
        logP = np.log1p(np.dot(X, cofactors))
        return np.sum(np.power(logY - logP, 2)) / count

    def gradient(self, cofactors):
        X = np.insert(self.data[:, 1:], 0, 1, axis=1)
        logY = np.log1p(self.data[:, 0])
        count = self.data.shape[0]
        P = np.dot(X, cofactors)
        return -np.sum(np.multiply(logY - np.log1p(P), np.reciprocal(1 + P)).reshape((count, 1)) * X, axis=0) / count

def makeData(count, cofactors, std=1.):
    cofactors = np.asarray(cofactors)
    dim = cofactors.shape[0]
    data = np.random.rand(count, dim - 1)
    offset = np.random.normal(0., std, count)
    targets = np.dot(data, cofactors[1:]) + cofactors[0] + offset

    return np.concatenate((targets.reshape((count, 1)), data), axis=1)

def linearRegression(data, cofactors=None, loss=LossP2, lr=0.01, steps=10000):
    lossEngine = loss(data)
    dim = data.shape[1]

    if cofactors is None:
        cofactors = np.zeros(dim)
    else:
        cofactors = np.asarray(cofactors)

    for i in range(steps):
        g = lossEngine.gradient(cofactors)
        np.subtract(cofactors, lr * g, cofactors)

    print(lossEngine.loss(cofactors))
    return cofactors
